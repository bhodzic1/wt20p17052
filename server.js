const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express();
const bodyParser = require('body-parser');
const db = require('./db.js');
const { Op } = require("sequelize");
 

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


db.sequelize.sync({ force: true }).then(function () {
    inicializacija().then(function () {
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });
});;

function inicializacija() {
    var listaPredmeta = [];
    var listaTipova = [];
    var listaDana = [];
    var listaGrupa = [];
    var listaStudenata = [];
    var grupaStudenata = [];
    return new Promise(function (resolve, reject) {
        listaTipova.push(db.tip.create({ naziv: 'Predavanje' }));
        listaTipova.push(db.tip.create({ naziv: 'Vježbe' }));
        listaDana.push(db.dan.create({ naziv: 'Ponedjeljak' }));
        listaDana.push(db.dan.create({ naziv: 'Utorak' }));
        listaDana.push(db.dan.create({ naziv: 'Srijeda' }));
        listaDana.push(db.dan.create({ naziv: 'Četvrtak' }));
        listaDana.push(db.dan.create({ naziv: 'Petak' }));
        listaGrupa.push(db.grupa.create({ naziv: 'RMAgrupa1' }));
        listaGrupa.push(db.grupa.create({ naziv: 'WTgrupa1' }));
        listaGrupa.push(db.grupa.create({ naziv: 'WTgrupa2' }));
        //listaPredmeta.push(db.predmet.create({ naziv: 'WT' }));
        //listaPredmeta.push(db.predmet.create({ naziv: 'RMA' }));
        Promise.all(listaGrupa).then(function(grupe) {
            var grupa1 = grupe.filter(function (a) { return a.naziv === 'RMAgrupa1' })[0];
            var grupa2 = grupe.filter(function (a) { return a.naziv === 'WTgrupa1' })[0];
            var grupa3 = grupe.filter(function (a) { return a.naziv === 'WTgrupa2' })[0];
            listaPredmeta.push(db.predmet.create({ naziv: 'RMA' }).then(function (k) {
                k.setGrupaPredmeta([grupa1]);
                return new Promise(function (resolve, reject) { resolve(k); });
            }));
            listaPredmeta.push(db.predmet.create({ naziv: 'WT' }).then(function (k) {
                k.setGrupaPredmeta([grupa2,grupa3]);
                return new Promise(function (resolve, reject) { resolve(k); });
            }));
            //listaGrupa.push(db.grupa.create({ naziv: 'WTgrupa1', predmetId: 1 }));
            //listaGrupa.push(db.grupa.create({ naziv: 'WTgrupa2', predmetId: 1 }));
        
        
        Promise.all(listaGrupa).then(function(grupe) {
            var grupa1 = grupe.filter(function (a) { return a.naziv === 'RMAgrupa1' })[0];
            var grupa2 = grupe.filter(function (a) { return a.naziv === 'WTgrupa1' })[0];
            var grupa3 = grupe.filter(function (a) { return a.naziv === 'WTgrupa2' })[0];
            listaStudenata.push(db.student.create({ ime: 'Neko Nekić', index: '12345' }).then(function (k) {
                k.addGrupe([grupa2]);
                return new Promise(function (resolve, reject) { resolve(k); });
            }));
            listaStudenata.push(db.student.create({ ime: 'Četvrti Neko', index: '18009' }).then(function(k) {
                k.addGrupe([grupa1]);
                return new Promise(function (resolve, reject) { resolve(k); });
            }));
        }).catch(function (err) { console.log("Studenti greska " + err); })
        }).catch(function (err) { console.log("Predmeti greska " + err); })
        
    })
}

app.use(express.static(__dirname + '/public'));
app.get('/', (req, res) => {
    res.send('Hello worlde!');
});

app.get('/aktivnost', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/aktivnost.html'));
})
app.get('/planiranjeNastavnik', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/planiranjeNastavnik.html'));
})
app.get('/podaciStudent', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/podaciStudent.html'));
})
app.get('/raspored', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/raspored.html'));
})



function jeLiPrazna(fileName, ignoreWhitespace = true) {
    return new Promise((resolve, reject) => {
        
        var data = fs.readFileSync(fileName, 'utf-8');
        resolve((!ignoreWhitespace && data.length == 0) || (ignoreWhitespace && !!String(data).match(/^\s*$/)))

    })
}


function pretvoriVrijeme(time) {
    var vrijeme = time.toString().split(':');  
    var sati = parseFloat(vrijeme[0]);
    var minute = parseFloat(vrijeme[1]/60); 
    return sati + minute; 
}

function vratiPredmete (predmeti) {
    var predmetiNiz = [];
    var i = 0;
    for (i = 0; i < predmeti.length; i++) {
        predmetiNiz.push({ "naziv": predmeti[i] });
    }
    return predmetiNiz;
}
app.get('/v1/predmeti', (req, res) => {
    jeLiPrazna('predmeti.txt').then((isEmpty) => {
        if (!isEmpty) {
            fs.readFile('predmeti.txt', function (err, data) {
                if (err) throw err;
                var predmeti = data.toString().split('\n');
                var predmetiNiz = [];
                var i = 0;

                for (i = 0; i < predmeti.length; i++) {
                    predmetiNiz.push({ "naziv": predmeti[i] });
                }

                res.json(predmetiNiz);
            })
        } else {
            var predmetiNiz = [];
            res.json(predmetiNiz);
        }
    });
});

function vratiAktivnosti (aktivnosti) {
    var aktivnostiNiz = [];
    var pomocniNiz = [];
    var i = 0;
    
    for (i = 0; i < aktivnosti.length; i++) {
        pomocniNiz = aktivnosti[i].toString().split(',');
        for (var j = 0; j < 1; j++) {
            aktivnostiNiz.push({
                "naziv": pomocniNiz[j++],
                "tip": pomocniNiz[j++],
                "pocetak": pretvoriVrijeme(pomocniNiz[j++]),
                "kraj": pretvoriVrijeme(pomocniNiz[j++]),
                "dan": pomocniNiz[j++]
            });
            
        }
    }
    
    return aktivnostiNiz;
}
function validacijaAktivnostiV2(aktivnostiNiz, vrijemePocetak, vrijemeKraj) {
    
    if (validacijaVremena(vrijemePocetak) && validacijaVremena(vrijemeKraj)) {
        vrijemePocetak = pretvoriVrijeme(vrijemePocetak);
        vrijemeKraj = pretvoriVrijeme(vrijemeKraj);
        for (var i = 0; i < aktivnostiNiz.length; i++) {
            if ((aktivnostiNiz[i].pocetak > vrijemePocetak) && (vrijemeKraj > aktivnostiNiz[i].pocetak || vrijemeKraj > aktivnostiNiz[i].kraj)) {
                return false;
            } else if (vrijemePocetak < pretvoriVrijeme("08:00") || vrijemeKraj < pretvoriVrijeme("08:00") || vrijemePocetak > pretvoriVrijeme("20:00") || vrijemeKraj > pretvoriVrijeme("20:00")) {
                return false;
            } else if ((aktivnostiNiz[i].pocetak < vrijemePocetak) && (vrijemePocetak < aktivnostiNiz[i].kraj)) {
                return false;
            } else if (vrijemePocetak >= vrijemeKraj || vrijemePocetak == aktivnostiNiz[i].pocetak || vrijemeKraj == aktivnostiNiz[i].kraj) {
                return false;
            }
        }
        return true;

    } else {
        return false;
    }
    

}

/*app.post('/v2/grupestudenata', function (req, res) {
    var body = req.body;
    
    db.grupa.findAll().then(function (grupe) {
        db.student.findAll().then(function (studenti) {
            db.predmet.findAll().then(function (predmeti) {
                var sviStudentiOk = true;
                var studentKojiNijeOk;
                for (var i = 0; i < body.length; i++) {
                    
                }
            })
            db.grupaStudenata.findOrCreate({ where: { grupaId: grupa.id, studentId: student.id } }).then(function (grupaStudenata) {
                res.json({ message: "Success" });
            })
            
        })
    });
    
});*/


app.get('/v1/aktivnosti', function(req, res) {
    jeLiPrazna('aktivnosti.txt').then((isEmpty) => {
        fs.readFile('aktivnosti.txt', function (err, data) {
            if (err) throw err;
            var aktivnosti = data.toString().split('\n');
            var aktivnostiNiz = [];
            var pomocniNiz = [];
            if (!isEmpty) {
                for (var i = 0; i < aktivnosti.length; i++) {
                    pomocniNiz = aktivnosti[i].toString().split(',');
                    for (var j = 0; j < 1; j++) { 
                        aktivnostiNiz.push({
                            "naziv": pomocniNiz[j++], 
                            "tip": pomocniNiz[j++],
                            "pocetak": pretvoriVrijeme(pomocniNiz[j++]),
                            "kraj": pretvoriVrijeme(pomocniNiz[j++]),
                            "dan": pomocniNiz[j++]
                        });
                    }
                }
            }
            res.json(aktivnostiNiz); 
        });

    })
});

app.get('/v1/predmet/:naziv/aktivnost/', (req, res) => {
    fs.readFile('aktivnosti.txt', function (err, data) {
        var naziv = req.params['naziv'];
        var aktivnosti = data.toString().split('\n');
        var aktivnostiNiz = vratiAktivnosti(aktivnosti);
        var reply = aktivnostiNiz.filter(aktivnost =>
            aktivnost.naziv == naziv
        )
        res.send(reply);
    });
});
function validacijaVremena (vrijeme) {
    vrijeme = vrijeme.toString().split(':');
    var sati = parseInt(vrijeme[0]);
    var minute = parseInt(vrijeme[1]);
    if (sati < 0 || sati > 24) {
        return false;
    } else if (minute != 0 && minute != 30) {
        return false;
    }
    
    return true;
}
function validacijaAktivnosti (dan, vrijemePocetak, vrijemeKraj) {
    var data = fs.readFileSync('aktivnosti.txt', 'utf8');
    if (validacijaVremena(vrijemePocetak) && validacijaVremena(vrijemeKraj)) {
        
        var aktivnosti = data.toString().split('\n'); 
        
        aktivnosti = vratiAktivnosti(aktivnosti);
        
        
        aktivnostiNiz = aktivnosti.filter(aktivnost => 
            aktivnost.dan == dan
        )
        
        vrijemePocetak = pretvoriVrijeme(vrijemePocetak);
        vrijemeKraj = pretvoriVrijeme(vrijemeKraj);
        for (var i = 0; i < aktivnostiNiz.length; i++) {
            if ((aktivnostiNiz[i].pocetak > vrijemePocetak) && (vrijemeKraj > aktivnostiNiz[i].pocetak || vrijemeKraj > aktivnostiNiz[i].kraj)) {
                return false;
            } else if (vrijemePocetak < pretvoriVrijeme("08:00") || vrijemeKraj < pretvoriVrijeme("08:00")) {
                return false;
            } else if ((aktivnostiNiz[i].pocetak < vrijemePocetak) && (vrijemePocetak < aktivnostiNiz[i].kraj)) {
                return false;
            } else if (vrijemePocetak >= vrijemeKraj || vrijemePocetak == aktivnostiNiz[i].pocetak || vrijemeKraj == aktivnostiNiz[i].kraj) {
                return false;
            }
        }
        return true;
        
    } else {
        aktivnostKorektna = false;
        return false;
    }
    
}

function jeLiUnutarRasporeda(vrijemePocetak, vrijemeKraj) {
    if (vrijemePocetak < 8 || vrijemeKraj < 8 || vrijemePocetak > 20 || vrijemeKraj > 20 || vrijemePocetak >= vrijemeKraj) {
        return false;
    } else return true;
}
app.post('/v1/aktivnost', function(req, res) {
    var message = { message: "Aktivnost nije validna!" };
    var body = req.body;
    var data = fs.readFileSync('aktivnosti.txt', 'utf8'); 
    data = data.toString().split('\n');
    var isValidPocetak = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(body['pocetak']);
    var isValidKraj = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(body['kraj']);
    
    jeLiPrazna('aktivnosti.txt').then((isEmpty) => {
        if (body['naziv'] != "" && isValidKraj && isValidPocetak) {
            if (isEmpty) {
                    if (jeLiUnutarRasporeda(pretvoriVrijeme(body['pocetak']), pretvoriVrijeme(body['kraj']))) {
                        var novaAktivnost = body['naziv'].toUpperCase() + "," + body['tip'] + "," + body['pocetak'] + "," + body['kraj'] + "," + body['dan'];
                        fs.appendFile('aktivnosti.txt', novaAktivnost, function (err) {
                            if (err) throw err;
                            res.send({ message: "Uspješno dodana aktivnost!" })
                            
                        })
                    } else res.send({ message: "Aktivnost nije validna!" });
            } else {
                if (validacijaAktivnosti(body['dan'], body['pocetak'], body['kraj']) == true && jeLiUnutarRasporeda(pretvoriVrijeme(body['pocetak']), pretvoriVrijeme(body['kraj']))) {
                    var novaAktivnost = '\n' +  body['naziv'].toUpperCase() + "," + body['tip'] + "," + body['pocetak'] + "," + body['kraj'] + "," + body['dan'];
                    fs.appendFile('aktivnosti.txt', novaAktivnost, function (err) {
                        if (err) throw err;
                        res.send({ message: "Uspješno dodana aktivnost!" })
                        
                    })
                } else res.send({ message: "Aktivnost nije validna!" }); 
            }
        } else res.send({ message: "Aktivnost nije validna!" });
    });
    
});

app.post('/v1/predmet', function (req, res) {
    var body = req.body;
    var data = fs.readFileSync('predmeti.txt', 'utf8');
    data = data.toString().split('\n');
    if (data.includes(body['naziv'])) {
        res.json({ message: "Naziv predmeta postoji!" })
    } else {
        jeLiPrazna('predmeti.txt').then((isEmpty) => {
            if (isEmpty) {
                var noviPredmet = body['naziv'].toUpperCase();
                fs.appendFile('predmeti.txt', noviPredmet, function (err) {
                    if (err) throw err;
                    res.json({ message: "Uspješno dodan predmet!" });
                })
            } else {
                var noviPredmet = '\n' + body['naziv'].toUpperCase();

                fs.appendFile('predmeti.txt', noviPredmet, function (err) {
                    if (err) throw err;
                    res.json({ message: "Uspješno dodan predmet!" });
                })
            }
        })
    }
});

app.delete('/v2/aktivnost/:naziv', (req, res) => {
    var naziv = req.params.naziv;
    db.aktivnost.findAll().then(function (aktivnosti) {
        db.aktivnost.destroy({ where: { naziv: naziv } }).then(function (aktivnost) {
            db.aktivnost.findAll().then(function (aktivnosti2) {
                if (aktivnosti.length != aktivnosti2.length) {
                    var message = { message: "Uspješno obrisana aktivnost!" };
                    res.send(message);
                } else {
                    res.send({ message: "Greška - aktivnost nije obrisana!" });
                }
            })
        })
    })
    
});
app.delete('/v1/predmet/:naziv', (req, res) => {
    var naziv = req.params.naziv;
    var data = fs.readFileSync('predmeti.txt', 'utf8');
    data = data.toString().split('\n');
    const index = data.indexOf(naziv); 
    if (index > -1) {
        data.splice(index, 1); 
        fs.writeFileSync('predmeti.txt', "");

        for (var i = 0; i < data.length; i++) {
            var predmet = data[i];
            if (i != data.length - 1) {
                predmet += '\n';
            }
            fs.appendFile('predmeti.txt', predmet, function (err) {
                if (err) throw err;
            })
        }
        var message = { message: "Uspješno obrisan predmet!" };
        res.send(message);
    } else {
        res.send({ message: "Greška - predmet nije obrisan!"});
    }
    
    
});

app.delete('/v1/aktivnost/:naziv', (req, res) => {
    var naziv = req.params.naziv;
    var data = fs.readFileSync('aktivnosti.txt', 'utf8');
    data = data.toString().split('\n');
    var noviNiz = [];
    for (var i = 0; i < data.length; i++) {
        var temp = data[i].split(',');
        if (temp[0] != naziv) {
            noviNiz.push(data[i]);
        }
    }
    if (noviNiz.length == data.length) {
        res.json({ message: "Greška - aktivnost nije obrisana!" })
    } else {
        fs.writeFileSync('aktivnosti.txt', "");

        for (var i = 0; i < noviNiz.length; i++) {
            var aktivnost = noviNiz[i];
            if (i != noviNiz.length - 1) {
                aktivnost += '\n';
            }
            fs.appendFile('aktivnosti.txt', aktivnost, function (err) {
                if (err) throw err;
            })
        }
        var message = { message: "Uspješno obrisana aktivnost!" };
        res.json(message);
    }

})


app.delete('/v1/all', (req, res) => {
    fs.writeFile('predmeti.txt', "", (err) => {
        if (err) res.json({ message: "Greška - sadržaj datoteka nije moguće obrisati!"});
        else {
            fs.writeFile('aktivnosti.txt', "", (err) => {
                if (err) res.json({ message: "Greška - sadržaj datoteka nije moguće obrisati!" });
                else res.json({ message: "Uspješno obrisan sadržaj datoteka!" });
            })
        }
    })
});

// v2 rute za tip
app.post('/v2/tip', function (req, res) {
    var body = req.body;
    db.tip.findAll({ where: { naziv: body['naziv'] } }).then(function (tip) {
        if (tip.length != 0) {
            res.json({ message: "Naziv tipa postoji!" })
        } else {
            db.tip.findOrCreate({ where: { naziv: body['naziv'] } }).then(function (predmet) {
                res.json({ message: "Uspješno dodan tip!" });
            })
        }
    })
})
app.get('/v2/tip', (req, res) => {
    var tipNiz = [];
    db.tip.findAll().then(function (tipovi) {
        for (var i = 0; i < tipovi.length; i++) {
            tipNiz.push({
                "id": tipovi[i].id,
                "naziv": tipovi[i].naziv
            })
        }
        res.json(tipNiz);
    });
});
app.put('/v2/tip', (req, res) => {
    var body = req.body;
    db.tip.update({ naziv: body['naziv'] }, { where: { id : body['id'] } }).then(function (tip) {
        res.json({ message: "Uspješno ažuriran tip!" })
    });
});
app.delete('/v2/tip', (req, res) => {
    var body = req.body;
    db.aktivnost.destroy({ where: { tipId : body['id'] } }).then(function(aktivnost) {
        db.tip.destroy({ where: { id: body['id'] } }).then(function (tip) {
            res.json({ message: "Uspješno obrisan tip!" });
        })
    })
});
// v2 rute za dan
app.post('/v2/dan', function (req, res) {
    var body = req.body;
    db.dan.findAll({ where: { naziv: body['naziv'] } }).then(function (dan) {
        if (dan.length != 0) {
            res.json({ message: "Naziv dana postoji!" })
        } else {
            db.dan.findOrCreate({ where: { naziv: body['naziv'] } }).then(function (predmet) {
                res.json({ message: "Uspješno dodan dan!" });
            })
        }
    })
});
app.get('/v2/dan', (req, res) => {
    var danNiz = [];
    db.dan.findAll().then(function (dani) {
        for (var i = 0; i < dani.length; i++) {
            danNiz.push({
                "id": dani[i].id,
                "naziv": dani[i].naziv
            })
        }
        res.json(danNiz);
    });
});
app.put('/v2/dan', (req, res) => {
    var body = req.body;
    db.dan.update({ naziv: body['naziv'] }, { where: { id: body['id'] } }).then(function (dan) {
        res.json({ message: "Uspješno ažuriran dan!" })
    });
});
app.delete('/v2/dan', (req, res) => {
    var body = req.body;
    db.aktivnost.destroy({ where: { danId: body['id'] } }).then(function (aktivnost) {
        db.dan.destroy({ where: { id: body['id'] } }).then(function (dan) {
            res.json({ message: "Uspješno obrisan dan!" });
        })
    })
});
// v2 rute za predmet
app.get('/v2/predmet', (req, res) => {
    var predmetiNiz = [];
    db.predmet.findAll().then(function (predmeti) {
        for (var i = 0; i < predmeti.length; i++) {
            predmetiNiz.push({
                "id": predmeti[i].id,
                "naziv": predmeti[i].naziv
            })
        }
        res.json(predmetiNiz);
    });
});
app.post('/v2/predmet', function (req, res) {
    var body = req.body;
    db.predmet.findAll({ where: { naziv: body['naziv'].toUpperCase() } }).then(function (predmet) {
        if (predmet.length != 0) {
            res.json({ message: "Naziv predmeta postoji!" })
        } else {
            db.predmet.findOrCreate({ where: { naziv: body['naziv'].toUpperCase() } }).then(function (predmet) {
                res.json({ message: "Uspješno dodan predmet!" });
            })
        }
    })
});

/*app.delete('/v2/predmet/:naziv', (req, res) => {
    var naziv = req.params.naziv;
    db.predmet.findAll().then(function (predmeti) {
        var idPredmeta;
        var idGrupe;
        for (var i = 0; i < predmeti.length; i++) {
            if (predmeti[i].naziv == naziv) {
                idPredmeta = predmeti[i].id;
            }
        }
        console.log(predmeti)
        db.grupa.findAll().then(function (grupe) {
            for (var i = 0; i < grupe.length; i++) {
                if (grupe[i].id == idPredmeta) {
                    idGrupe = grupe[i].id;
                }
            }
            db.aktivnost.destroy({ where: { predmetId: idPredmeta } }).then(function (aktivnost) {
                
                db.grupa.destroy({ where: { predmetId: idPredmeta } }).then(function (grupa) {
                    db.predmet.destroy({ where: { id: idPredmeta } }).then(function (dan) {
                        db.predmet.findAll().then(function (predmeti2) {
                            console.log(predmeti)
                            if (predmeti.length != predmeti2.length) {
                                var message = { message: "Uspješno obrisan predmet!" };
                                res.send(message);
                            } else {
                                res.send({ message: "Greška - predmet nije obrisan!" });
                            }
                        })
                    })
                })
                
            })
        })
    })
});*/

app.delete('/v2/predmet/:naziv', (req, res) => {
    var naziv = req.params.naziv;
     
    db.aktivnost.destroy({ where: { naziv: naziv } }).then(function() {
        db.predmet.findAll({ where : { naziv : naziv } }).then(function(predmet) {
            
            db.grupa.destroy({ where: { predmetId : predmet[0].id } }).then(function(grupa) {
                db.predmet.destroy({ where: { naziv: naziv } }).then(function(predmet) {
                    db.predmet.findAll({ where: { naziv: naziv } }).then(function (predmet) {
                        
                    })
                    var message = { message: "Uspješno obrisan predmet!" };
                    res.send(message);
                })
            })
        })
    })
})
app.put('/v2/predmet', (req, res) => {
    var body = req.body;
    db.aktivnost.update({ naziv : body['naziv'] }, { where: { predmetId : body['id'] } }).then(function (aktivnost) {
        db.predmet.update({ naziv: body['naziv'] }, { where: { id: body['id'] } }).then(function (dan) {
            res.json({ message: "Uspješno ažuriran predmet!" })
        });
    });
});
// v2 rute za grupe 
app.get('/v2/grupa', (req, res) => {
    var grupeNiz = [];
    db.grupa.findAll().then(function (grupe) {
        for (var i = 0; i < grupe.length; i++) {
            grupeNiz.push({
                "id": grupe[i].id,
                "naziv": grupe[i].naziv,
                "predmet": grupe[i].predmetId
            })
        }
        res.json(grupeNiz);
    });
    
});
app.post('/v2/grupa', function (req, res) {
    var body = req.body;
    db.grupa.findAll({ where: { naziv: body['naziv'] } }).then(function (grupa) {
        if (grupa.length != 0) {
            res.json({ message: "Naziv grupe postoji!" })
        } else {
            db.grupa.findOrCreate({ where: { naziv: body['naziv'] } }).then(function (grupa) {
                res.json({ message: "Uspješno dodana grupa!" });
            })
        }
    })
});
app.delete('/v2/grupa', (req, res) => {
    var body = req.body;
    db.grupa.destroy({ where: { id: body['id'] } }).then(function (aktivnost) {
        var message = { message: "Uspješno obrisana grupa!" };
        res.send(message);
    })
});
app.put('/v2/grupa', (req, res) => {
    var body = req.body;
    db.grupa.update({ naziv: body['naziv'] }, { where: { id: body['id'] } }).then(function (grupa) {
        res.json({ message: "Uspješno ažurirana grupa!" })
    });
});
// v2 rute za aktivnost
app.get('/v2/aktivnost', function (req, res) {
    var aktivnostiNiz = [];
    db.aktivnost.findAll().then(function (aktivnosti) {
        db.dan.findAll().then(function (dani) {
            db.tip.findAll().then(function (tipovi) {
                for (var i = 0; i < aktivnosti.length; i++) {
                    var dan = "";
                    var tip = "";
                    for (var j = 0; j < dani.length; j++) {
                        if (dani[j].id == aktivnosti[i].danId) {
                            dan = dani[j].naziv;
                        }
                    }
                    for (var j = 0; j < tipovi.length; j++) {
                        if (tipovi[j].id == aktivnosti[i].tipId) {
                            tip = tipovi[j].naziv;
                        }
                    }
                    aktivnostiNiz.push({
                        "id": aktivnosti[i].id,
                        "naziv": aktivnosti[i].naziv,
                        "tip": tip,
                        "pocetak": aktivnosti[i].pocetak,
                        "kraj": aktivnosti[i].kraj,
                        "dan": dan
                    })
                }
                res.json(aktivnostiNiz);
            })
        })

    });
});
app.post('/v2/aktivnost', function (req, res) {
    var body = req.body;
    var isValidPocetak = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(body['pocetak']);
    var isValidKraj = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(body['kraj']);

    if (body['naziv'] != "" && isValidKraj && isValidPocetak) {
        db.dan.findAll({ where: { naziv: body['dan'] } }).then(function (dani) {
            db.aktivnost.findAll({ where: { danId: dani[0].id } }).then(function (aktivnosti) {
                if (validacijaAktivnostiV2(aktivnosti, body['pocetak'], body['kraj']) == false || jeLiUnutarRasporeda(pretvoriVrijeme(body['pocetak']), pretvoriVrijeme(body['kraj'])) == false) {
                    res.json({ message: "Aktivnost nije validna!" });
                } else {
                    db.predmet.findAll({ where: { naziv: body['naziv'].toUpperCase() } }).then(function (predmet) {
                        db.dan.findOrCreate({ where: { naziv: body['dan'] } }).then(function (dan) {
                            db.tip.findOrCreate({ where: { naziv: body['tip'] } }).then(function (tip) {
                                db.aktivnost.findOrCreate({ where: { naziv: body['naziv'].toUpperCase(), pocetak: pretvoriVrijeme(body['pocetak']), kraj: pretvoriVrijeme(body['kraj']), predmetId: predmet[0].id, danId: dan[0].id, tipId: tip[0].id } }).then(function (aktivnost) {
                                    res.send({ message: "Uspješno dodana aktivnost!" });
                                });
                            });
                        });
                    });
                }
            })
        })
    } else res.send({ message: "Aktivnost nije validna!" });
});
app.delete('/v2/aktivnost', (req, res) => {
    var body = req.body;
    db.aktivnost.destroy({ where: { id: body['id'] } }).then(function (aktivnost) {
        var message = { message: "Uspješno obrisana aktivnost!" };
        res.send(message);
    })
});
app.put('/v2/aktivnost', (req, res) => {
    var body = req.body;
    db.aktivnost.update({ naziv: body['naziv'], pocetak: body['pocetak'], kraj: body['kraj'], predmetId: body['predmetId'], danId: body['danId'] }, { where: { id: body['id'] } }).then(function (grupa) {
        res.json({ message: "Uspješno ažurirana aktivnost!" })
    });
});
// v2 rute za studenta
app.post('/v2/student', function (req, res) {
    var body = req.body; 
    db.student.findOne({ where: { ime: body['ime'], index: body['index'] } }).then(function (student) {
        if (student) {
            db.grupa.findOne({ where: { id : body['grupa'] } }).then(function (grupa) {
                db.grupa.findAll({ where: { predmetId: grupa.predmetId } }).then(function(grupe) {
                    student.getGrupe().then(function (resSet) {
                        var postojiGrupa = false;
                        var noveGrupe = [];
                        for (var i = 0; i < resSet.length; i++) {
                            if (resSet[i].predmetId == grupa.predmetId && resSet[i].naziv != grupa.naziv) {
                                //student.setGrupe([grupa]);
                                postojiGrupa = true;
                            } else {
                                noveGrupe.push(resSet[i]);
                            }
                        }
                        if (postojiGrupa) {
                            noveGrupe.push(grupa)
                            student.setGrupe(noveGrupe);
                        } else {
                            noveGrupe.push(grupa)
                            student.setGrupe(noveGrupe);
                        }
                        res.status(200).send("OK");
                    })
                })
            })
        }
        else {
            db.student.findOne({ where: { index: body['index'] } }).then(function (student) {
                if (student) res.send("Student " + body['ime'] + " nije kreiran jer postoji student " + student.ime + " sa istim indexom " + body['index']);
                else {
                    db.student.create({ ime: body['ime'], index: body['index'] }).then(function(student) {
                        student.addGrupe([body['grupa']]);
                        res.status(200).send("OK");
                    })
                }
            })
        }
    })
});
app.get('/v2/student', (req, res) => {
    var studentiNiz = [];
    var grupeNiz = [];
    db.student.findAll().then(function (studenti) {
        for (var i = 0; i < studenti.length; i++) {
            studentiNiz.push({
                "id": studenti[i].id,
                "ime": studenti[i].ime,
                "index": studenti[i].index
            })
        }
        res.json(studentiNiz);
    });
});
app.put('/v2/student', (req, res) => {
    var body = req.body;
    db.student.update({ ime: body['ime'], index: body['index']}, { where: { id: body['id'] } }).then(function (grupa) {
        res.json({ message: "Uspješno ažuriran student!" })
    });
});
app.delete('/v2/student', (req, res) => {
    var body = req.body;
    db.student.destroy({ where: { id: body['id'] } }).then(function (student) {
        var message = { message: "Uspješno obrisan student!" };
        res.send(message);
    })
});


app.listen(3000, () => console.log('App listening on port 3000!'));