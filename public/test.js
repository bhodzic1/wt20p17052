let sinon = require('sinon');
let fs = require('fs');
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
let server = require('./testModul');

describe('Aktivnost', function() {
    let testniPodaci = fs.readFileSync('testniPodaci.txt', 'utf8').toString().split('\n');
    beforeEach(function () {
        this.xhr = sinon.useFakeXMLHttpRequest();

        this.requests = [];
        this.xhr.onCreate = function (xhr) {
            this.requests.push(xhr);
        }.bind(this);
    });
    afterEach(function () {
        this.xhr.restore();
    });
    
    for (var i = 0; i < testniPodaci.length; i++) {
        let testnaLinija = testniPodaci[i].split(',')
        if (testnaLinija[0] == 'GET' && testnaLinija[1] == '/predmeti') {
            describe('vratiPredmete()', function () {
                it('Treba vratiti niz predmeta', function (done) {
                    var nizPredmeta = [];
                    if (testnaLinija[3] != '[]') {
                        for (var j = 3; j < testnaLinija.length; j++) {
                            var poruka = testnaLinija[j].replace(/\\/g, "").replace(/”/g, '"');
                            poruka = poruka.replace(/\[/g, "").replace(/\]/g, "");
                            nizPredmeta.push(JSON.parse(poruka));
                        }
                    }
                    server.vratiPredmete(function (result) {
                        result.should.deep.equal(nizPredmeta);
                        done();
                    });
                });
            });
            
        } else if (testnaLinija[0] == 'GET' && testnaLinija[1] == '/aktivnosti') {
            describe('vratiAktivnosti()', function () {
                it('Treba vratiti niz aktivnosti', function (done) {
                    var nizAktivnosti = [];
                    if (testnaLinija[3] != '[]') {
                        for (var j = 3; j < testnaLinija.length; j++) {
                            var predmet = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"').replace(/\{/g, "").replace(/\[/g, "").replace(/\]/g, "");
                            var tip = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"');
                            var pocetak = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"');
                            var kraj = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"');
                            var dan = testnaLinija[j].replace(/\\/g, "").replace(/”/g, '"').replace(/\}/g, "").replace(/\]/g, "");
                            var aktivnostString = "{" + predmet + "," + tip + "," + pocetak + "," + kraj + "," + dan + "}";

                            var aktivnost = JSON.parse(aktivnostString);
                            
                            nizAktivnosti.push(aktivnost);
                        }
                    }
                    server.vratiAktivnosti(function (result) {
                        result.should.deep.equal(nizAktivnosti);
                        done();
                    });
                });
            });

        } else if (testnaLinija[0] == 'GET' && testnaLinija[1] == '/predmet') {
            describe('vratiAktivnostZaZadaniPredmet()', function () {
                it('Treba vratiti niz aktivnosti za zadani predmet', function (done) {
                    var predmet = testnaLinija[2].replace(/\\/g, '').replace(/”/g, '"')
                    predmetJSON = JSON.parse(predmet);
                    var nizAktivnosti = [];
                    if (testnaLinija[3] != '[]') {
                        for (var j = 3; j < testnaLinija.length; j++) {
                            var predmet = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"').replace(/\{/g, "").replace(/\[/g, "").replace(/\]/g, "");
                            var tip = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"');
                            var pocetak = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"');
                            var kraj = testnaLinija[j++].replace(/\\/g, "").replace(/”/g, '"');
                            var dan = testnaLinija[j].replace(/\\/g, "").replace(/”/g, '"').replace(/\}/g, "").replace(/\]/g, "");
                            var aktivnostString = "{" + predmet + "," + tip + "," + pocetak + "," + kraj + "," + dan + "}";

                            var aktivnost = JSON.parse(aktivnostString);

                            nizAktivnosti.push(aktivnost);
                        }
                    }
                    server.vratiAktivnostZaZadaniPredmet(predmetJSON, function (result) {
                        result.should.deep.equal(nizAktivnosti);
                        done();
                    });
                });
            });

        } else if (testnaLinija[0] == 'DELETE' && testnaLinija[1] == '/all') {
            describe('obrisiSvePredmete()', function() {
                it('Treba obrisati sve predmete', function (done) {
                    var poruka = testnaLinija[3].replace(/\\/g, '').replace(/”/g, '"');
                    server.obrisiSve(function (result) {
                        result = JSON.stringify(result);
                        result.should.deep.equal(poruka);
                        done();
                    });
                });
            });
        } else if (testnaLinija[0] == 'DELETE' && testnaLinija[1] == '/predmet') {
            describe('obrisiPredmet()', function () {
                it('Treba obrisati zadani predmet', function (done) {
                    var predmet = testnaLinija[2].replace(/\\/g, '').replace(/”/g, '"')
                    predmetJSON = JSON.parse(predmet);
                    var poruka = testnaLinija[3].replace(/\\/g, '').replace(/”/g, '"');
                    server.obrisiPredmet(predmetJSON, function (result) {
                        result = JSON.stringify(result);
                        result.should.deep.equal(poruka);
                        done();
                    });
                });
            });
        } else if (testnaLinija[0] == 'DELETE' && testnaLinija[1] == '/aktivnost') {
            describe('obrisiAktivnost()', function () {
                it('Treba obrisati zadatu aktivnost', function (done) {
                    var predmet = testnaLinija[2].replace(/\\/g, '').replace(/”/g, '"')
                    predmetJSON = JSON.parse(predmet);
                    var poruka = testnaLinija[3].replace(/\\/g, '').replace(/”/g, '"');
                    server.obrisiAktivnost(predmetJSON, function (result) {
                        result = JSON.stringify(result);
                        result.should.deep.equal(poruka);
                        done();
                    });
                });
            });
        } else if (testnaLinija[0] == 'POST' && testnaLinija[1] == '/predmet') {
            describe('dodajPredmet()', function () {
                it('Treba dodati predmet', function (done) {
                    var poruka = testnaLinija[3].replace(/\\/g, "").replace(/”/g, '"');
                    var predmet = testnaLinija[2].replace(/\\/g, "").replace(/”/g, '"');
                    var predmetJSON = JSON.parse(predmet);
                    server.dodajPredmet(predmet, function (result) {
                        result = JSON.stringify(result);
                        result.should.deep.equal(poruka);
                        done();
                    });
                });
            });
        } else if (testnaLinija[0] == 'POST' && testnaLinija[1] == '/aktivnost') {
            describe('dodajAktivnost()', function () {
                it('Treba dodati aktivnost', function (done) {
                    var aktivnost = null;
                    if (testnaLinija[2] != null) {
                        var predmet = testnaLinija[2].replace(/\\/g, "").replace(/”/g, '"').replace(/\{/g, "");
                        var tip = testnaLinija[3].replace(/\\/g, "").replace(/”/g, '"');
                        var pocetak = testnaLinija[4].replace(/\\/g, "").replace(/”/g, '"');
                        var kraj = testnaLinija[5].replace(/\\/g, "").replace(/”/g, '"');
                        var dan = testnaLinija[6].replace(/\\/g, "").replace(/”/g, '"').replace(/\}/g, "");
                        var aktivnostString = "{" + predmet + "," + tip + "," + pocetak + "," + kraj + "," + dan + "}";
                        
                        aktivnost = JSON.parse(aktivnostString);
                    }
                    var poruka = testnaLinija[7].replace(/\\/g, "").replace(/”/g, '"');
                    
                    server.dodajAktivnost(aktivnost, function (result) {
                        result = JSON.stringify(result);
                        result.should.deep.equal(poruka);
                        done();
                    });
                });
            });
        }
    }
    
})