

var predmetiNiz = [];
var predmetiAjax = new XMLHttpRequest();

predmetiAjax.onreadystatechange = function() {
    if (predmetiAjax.readyState == 4 && predmetiAjax.status == 200) {
        predmetiNiz = JSON.parse(this.responseText);
        for(var i = 0; i < predmetiNiz.length; i++) {
            var p = document.createElement("p");
            p.innerHTML = predmetiNiz[i].naziv;
            document.getElementById("predmetiLista").appendChild(p);  
        }
    }
}


var aktivnostiNiz = [];
var aktivnostiAjax = new XMLHttpRequest();
  
aktivnostiAjax.onreadystatechange = function () {
    if (aktivnostiAjax.readyState == 4 && aktivnostiAjax.status == 200) {
        aktivnostiNiz = JSON.parse(this.responseText);
        for (var i = 0; i < aktivnostiNiz.length; i++) {
            var p = document.createElement("p");
            p.innerHTML = aktivnostiNiz[i].naziv + "," + aktivnostiNiz[i].tip + "," + aktivnostiNiz[i].pocetak + "," + aktivnostiNiz[i].kraj + "," + aktivnostiNiz[i].dan;
            document.getElementById("aktivnostiLista").appendChild(p);
        }
    }
}

window.onload = function () {
    
    predmetiAjax.open("GET", "http://localhost:3000/v2/predmet", true);
    predmetiAjax.send();
    aktivnostiAjax.open("GET", "http://localhost:3000/v2/aktivnost", true);
    aktivnostiAjax.send();
} 

function refresh() {
    document.getElementById("predmetiLista").removeChild("p");
    document.getElementById("aktivnostiLista").removeChild("p");
    
}
/*function submitAktivnost() {

    var naziv = document.getElementById("naziv").value;
    
    var temp = false;
    for (var i = 0; i < predmetiNiz.length; i++) {
        if (predmetiNiz[i].naziv == naziv.toUpperCase())
            temp = true;
    }

    if (temp == false) {
        
        var predmet = {
            "naziv": naziv.toUpperCase()
        };
        var predmetJSON = JSON.stringify(predmet);
        var dodajPredmet = new XMLHttpRequest();
        dodajPredmet.open("POST", "http://localhost:3000/v2/predmet", true);
        dodajPredmet.setRequestHeader("Content-Type", "application/json");
        dodajPredmet.onreadystatechange = function () {
            if (dodajPredmet.readyState == 4 && dodajPredmet.status == 200) {
                var nesto = JSON.parse(this.responseText).message; 
                console.log("bernes")
            }
            dodajNovuAktivnost(false);
        }
        dodajPredmet.send(predmetJSON);
        
    } else {
        console.log("bernes2")
        dodajNovuAktivnost(true)
    }
    
}

function dodajNovuAktivnost (temp) {
    var naziv = document.getElementById("naziv").value;
    var tip = document.getElementById("tip").value;
    var pocetak = document.getElementById("pocetak").value;
    var kraj = document.getElementById("kraj").value;
    var dan = document.getElementById("dan").value;
    var aktivnost = {
        "naziv": naziv,
        "tip": tip,
        "pocetak": pocetak,
        "kraj": kraj,
        "dan": dan
    }
    var aktivnostJSON = JSON.stringify(aktivnost);
    var dodajAktivnost = new XMLHttpRequest();
    dodajAktivnost.open("POST", "http://localhost:3000/v2/aktivnost", true);
    dodajAktivnost.setRequestHeader("Content-Type", "application/json");
    dodajAktivnost.getResponseHeader("Content-Type", "application/json");
    dodajAktivnost.onreadystatechange = function () {
        if (dodajAktivnost.readyState == 4 && dodajAktivnost.status == 200) {
            var reply = JSON.parse(this.responseText);
            if (reply.message == "Aktivnost nije validna!" && temp == false) {
                obrisiPredmet(naziv.toUpperCase());
            }
        }
    }
    dodajAktivnost.send(aktivnostJSON);  
}
*/
function obrisiPredmet (naziv) {
    var obrisiPredmet = new XMLHttpRequest();
    obrisiPredmet.open("DELETE", "http://localhost:3000/v2/predmet/" + naziv, true);
    obrisiPredmet.setRequestHeader("Content-Type", "application/json");
    obrisiPredmet.getResponseHeader("Content-Type", "application/json");
    obrisiPredmet.onreadystatechange = function () {
        if (obrisiPredmet.readyState == 4 && obrisiPredmet.status == 200) {
            var poruka = JSON.parse(this.responseText);
        }
    }
    obrisiPredmet.send();
}

function submitAktivnost () {
    var naziv = document.getElementById("naziv").value;
    var tip = document.getElementById("tip").value;
    var pocetak = document.getElementById("pocetak").value;
    var kraj = document.getElementById("kraj").value;
    var dan = document.getElementById("dan").value;
    var aktivnost = {
        "naziv": naziv,
        "tip": tip,
        "pocetak": pocetak,
        "kraj": kraj,
        "dan": dan
    }
    var aktivnostJSON = JSON.stringify(aktivnost);
    var predmetNaziv = document.getElementById("naziv").value;
    predmetNaziv = predmetNaziv.toUpperCase();
    var predmet = {
        "naziv": predmetNaziv.toUpperCase()
    };
    var predmetJSON = JSON.stringify(predmet);
    var dodajPredmet = new XMLHttpRequest();
    dodajPredmet.onreadystatechange = function () {
        if (dodajPredmet.readyState == 4 && dodajPredmet.status == 200) {
            var porukaPredmeta = JSON.parse(this.responseText).message;
            var dodajAktivnost = new XMLHttpRequest();
            //dodajAktivnost.getResponseHeader("Content-Type", "application/json");
            dodajAktivnost.onreadystatechange = function () {
                if (dodajAktivnost.readyState == 4 && dodajAktivnost.status == 200) {
                    var reply = JSON.parse(this.responseText);
                    
                    if (reply.message == "Aktivnost nije validna!" && porukaPredmeta == "Uspješno dodan predmet!") {
                        
                        var obrisiPredmet = new XMLHttpRequest();
                        obrisiPredmet.open("DELETE", "http://localhost:3000/v2/predmet/" + predmetNaziv, true);
                        obrisiPredmet.setRequestHeader("Content-Type", "application/json");
                        obrisiPredmet.send();
                        
                    }
                    
                }
            }
            dodajAktivnost.open("POST", "http://localhost:3000/v2/aktivnost", true);
            dodajAktivnost.setRequestHeader("Content-Type", "application/json");
            dodajAktivnost.send(aktivnostJSON);
        }
    }
    dodajPredmet.open("POST", "http://localhost:3000/v2/predmet", true);
    dodajPredmet.setRequestHeader("Content-Type", "application/json");
    dodajPredmet.send(predmetJSON);
}