var grupeNiz = [];
var grupeAjax = new XMLHttpRequest();
grupeAjax.open("GET", "http://localhost:3000/v2/grupa", true);
grupeAjax.onreadystatechange = function () {
    if (grupeAjax.readyState == 4 && grupeAjax.status == 200) {
        grupeNiz = JSON.parse(this.responseText);
        for (var i = 0; i < grupeNiz.length; i++) {
            var option = document.createElement("option");
            var optionText = document.createTextNode(grupeNiz[i].naziv);
            option.appendChild(optionText);
            option.setAttribute('value', grupeNiz[i].naziv)
            option.value = grupeNiz[i].naziv;
            document.getElementById("grupa").appendChild(option);
        }
    }
}
grupeAjax.send();

function dodaj() {
    var grupaText = document.getElementById("grupa").value;
    var grupaId;
    var predmetId;
    for (var i = 0; i < grupeNiz.length; i++) {
        if (grupaText == grupeNiz[i].naziv) {
            grupaId = grupeNiz[i].id;
            predmetId = grupeNiz[i].predmet;
        }
    }
    var poruka = "";
    var nizPoruka = [];
    var unos = document.getElementById("unos").value;
    var nizStudenata = unos.split('\n');
    var nizStudenataJSON = [];
    var grupa = {
        "id": grupaId,
        "naziv": grupaText,
        "predmet": predmetId
    }
    document.getElementById("unos").value = "";
    var xhr = [];
    var i;
    for (i = 0; i < nizStudenata.length; i++) {
        (function(i) {
        var temp = nizStudenata[i].split(',');
        var student = {
            "ime" : temp[0],
            "index" : temp[1],
            "grupa" : grupaId
        }
            if (student.ime != "" && student.index != "") {
                var studentiJSON = JSON.stringify(student);
                var dodajGrupuStudenata = new XMLHttpRequest();
                dodajGrupuStudenata.open("POST", "http://localhost:3000/v2/student", true);
                dodajGrupuStudenata.setRequestHeader("Content-Type", "application/json");
                //dodajGrupuStudenata.getResponseHeader("Content-Type", "application/json");
                dodajGrupuStudenata.onreadystatechange = function () {
                    if (dodajGrupuStudenata.readyState == 4 && dodajGrupuStudenata.status == 200) {
                        poruka = this.responseText;
                        nizPoruka.push(poruka);
                        
                        if (poruka != "OK") {
                            document.getElementById("unos").value += poruka + "\n";
                        }
                        
                    }
                }

                dodajGrupuStudenata.send(studentiJSON);
            }
        })(i);
    }
    
}