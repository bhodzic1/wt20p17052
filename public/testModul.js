var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var server = {
    obrisiSve: function(callback) {
        var obrisiPredmet = new XMLHttpRequest();
        obrisiPredmet.open("DELETE", "http://localhost:3000/all", true);
        obrisiPredmet.setRequestHeader("Content-Type", "application/json");
        obrisiPredmet.getResponseHeader("Content-Type", "application/json");
        obrisiPredmet.onreadystatechange = function () {
            if (obrisiPredmet.readyState == 4 && obrisiPredmet.status == 200) {
                var poruka = JSON.parse(this.responseText);
                callback(poruka);
            }
        }
        obrisiPredmet.send();
    },
    vratiPredmete: function(callback) {
        var predmetiNiz = [];
        var predmetiAjax = new XMLHttpRequest();
        predmetiAjax.open("GET", "http://localhost:3000/predmeti", true);
        predmetiAjax.onreadystatechange = function () {
            if (predmetiAjax.readyState == 4 && predmetiAjax.status == 200) {
                predmetiNiz = JSON.parse(this.responseText);
                callback(predmetiNiz)
            }
        }
        predmetiAjax.send();
    },
    vratiAktivnosti: function(callback) {
        var aktivnostiNiz = [];
        var aktivnostiAjax = new XMLHttpRequest();
        aktivnostiAjax.open("GET", "http://localhost:3000/aktivnosti", true);
        aktivnostiAjax.onreadystatechange = function () {
            if (aktivnostiAjax.readyState == 4 && aktivnostiAjax.status == 200) {
                aktivnostiNiz = JSON.parse(this.responseText);
                callback(aktivnostiNiz)
            }
        }
        aktivnostiAjax.send();
    },
    dodajPredmet: function(data, callback) {
        var dodajPredmet = new XMLHttpRequest();
        dodajPredmet.open("POST", "http://localhost:3000/predmet", true);
        dodajPredmet.setRequestHeader("Content-Type", "application/json");
        dodajPredmet.onreadystatechange = function () {
            if (dodajPredmet.readyState == 4 && dodajPredmet.status == 200) {
                var poruka = JSON.parse(this.responseText);
                callback(poruka);
            }
        }

        dodajPredmet.send(data);
    },
    dodajAktivnost: function(data, callback) {
        var aktivnostJSON = JSON.stringify(data);
        var dodajAktivnost = new XMLHttpRequest();
        dodajAktivnost.open("POST", "http://localhost:3000/aktivnost", true);
        dodajAktivnost.setRequestHeader("Content-Type", "application/json");
        dodajAktivnost.getResponseHeader("Content-Type", "application/json");
        dodajAktivnost.onreadystatechange = function () {
            if (dodajAktivnost.readyState == 4 && dodajAktivnost.status == 200) {
                var poruka = JSON.parse(this.responseText);
                /*if (reply.message == "Aktivnost nije validna!" && temp == false) {
                    obrisiPredmet(naziv);
                }*/
                callback(poruka);
            }
        }
        dodajAktivnost.send(aktivnostJSON);
    },
    obrisiPredmet: function(data, callback) {
        var obrisiPredmet = new XMLHttpRequest();
        obrisiPredmet.open("DELETE", "http://localhost:3000/predmet/" + data.naziv, true);
        obrisiPredmet.setRequestHeader("Content-Type", "application/json");
        obrisiPredmet.getResponseHeader("Content-Type", "application/json");
        obrisiPredmet.onreadystatechange = function () {
            if (obrisiPredmet.readyState == 4 && obrisiPredmet.status == 200) {
                var poruka = JSON.parse(this.responseText);
                callback(poruka);
            }
        }
        obrisiPredmet.send();
    }, 
    vratiAktivnostZaZadaniPredmet: function(data, callback) {
        var aktivnostiNiz = [];
        var aktivnostiAjax = new XMLHttpRequest();
        aktivnostiAjax.open("GET", "http://localhost:3000/predmet/" + data.naziv + "/aktivnost", true);
        aktivnostiAjax.onreadystatechange = function () {
            if (aktivnostiAjax.readyState == 4 && aktivnostiAjax.status == 200) {
                aktivnostiNiz = JSON.parse(this.responseText);
                callback(aktivnostiNiz)
            }
        }
        aktivnostiAjax.send();
    },
    obrisiAktivnost: function(data, callback) {
        var obrisiAktivnost = new XMLHttpRequest();
        obrisiAktivnost.open("DELETE", "http://localhost:3000/aktivnost/" + data.naziv, true);
        obrisiAktivnost.setRequestHeader("Content-Type", "application/json");
        obrisiAktivnost.getResponseHeader("Content-Type", "application/json");
        obrisiAktivnost.onreadystatechange = function () {
            if (obrisiAktivnost.readyState == 4 && obrisiAktivnost.status == 200) {
                var poruka = JSON.parse(this.responseText);
                callback(poruka);
            }
        }
        obrisiAktivnost.send();
    }
}
module.exports = server;