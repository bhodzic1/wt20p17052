const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2017052", "root", "", { host: "127.0.0.1", dialect: "mysql", logging: false });
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//import modela
db.predmet = sequelize.import(__dirname + '/predmet.js');
db.student = sequelize.import(__dirname + '/student.js');
db.grupa = sequelize.import(__dirname + '/grupa.js');
db.dan = sequelize.import(__dirname + '/dan.js');
db.tip = sequelize.import(__dirname + '/tip.js');
db.aktivnost = sequelize.import(__dirname + '/aktivnost.js');
//db.grupaStudenata = sequelize.import(__dirname + '/grupaStudenata.js');

//Relacije
db.predmet.hasMany(db.grupa, { as: 'grupaPredmeta' });
db.predmet.hasMany(db.aktivnost, { as: 'predmet'});
db.dan.hasMany(db.aktivnost, { as: "dan" });
db.tip.hasMany(db.aktivnost, { as: 'tip' });

db.grupaStudenata = db.student.belongsToMany(db.grupa, { as: 'grupe', through: 'grupa_student', foreignKey: 'studentId' });
db.grupa.belongsToMany(db.student, { as: 'studenti', through: 'grupa_student', foreignKey: 'grupaId' });

/*db.student.hasMany(db.grupaStudenata, { as: "student" });
db.grupa.hasMany(db.grupaStudenata, { as: "grupa" });*/


module.exports = db;